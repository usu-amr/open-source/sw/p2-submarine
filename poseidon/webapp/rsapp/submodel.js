// ------------------------------------------------
// BASIC SETUP
// ------------------------------------------------
//   ___             //////
//   (__|_______--/| /  ////
// ___/  +--+| |-\| (>   )//
// |_|ooo+--+|_|    /_    //
//   '-------'       |_,   /
//                    [___/
// 
// ------------------------------------------------
// Create an empty scene
var scene = new THREE.Scene();

// Create a basic perspective camera
var camera = new THREE.PerspectiveCamera( 75,5/3, 0.1, 1000 );
camera.position.x = 3;
camera.position.y = 0.5;
camera.position.z = 1;
camera.lookAt(0,0,0);
// Create a renderer with Antialiasing
var renderer = new THREE.WebGLRenderer({antialias:true});

var controls = new THREE.OrbitControls(camera, renderer.domElement);
// Configure renderer clear color
renderer.setClearColor("#ffffff");
let submodel = document.getElementById("submodel");

// Configure renderer size
renderer.setSize( 500, 300 );

var radius = 2.3;
var radials = 16;
var circles = 12;
var divisions = 64;

var gridHelper = new THREE.PolarGridHelper( radius, radials, circles, divisions, "#777777", "#bbbbbb" );
//scene.add( gridHelper );

var worldAxis = new THREE.AxesHelper(20);
scene.add(worldAxis);

// Append Renderer to DOM
document.getElementById("submodel").appendChild( renderer.domElement );

// ------------------------------------------------
// Load Submarine Model
// ------------------------------------------------
// 
// 
//                ___________
//                | .   '   |
//            __  |   ___'  |     ____
//           ():) | . | ;|  |    ():__)
//     ________|__|_________|______|_|____
// 
// ------------------------------------------------


let submesh;
let baseQuat = (new THREE.Quaternion(0,0,0,0)).setFromEuler(new THREE.Euler( 0, 0, Math.PI/2, 'XYZ' ));

var loader = new THREE.STLLoader();
loader.load( '/rsapp/mini.stl', function ( geometry ) {
  var material = new THREE.MeshPhongMaterial( { color: 0x008080, specular: 0x111111, shininess: 50 } );
  var mesh = new THREE.Mesh( geometry, material );

  mesh.position.set( 0, 0, 0 );
  mesh.setRotationFromQuaternion(baseQuat);
  mesh.scale.set( 0.5, 0.5, 0.5 );

  mesh.castShadow = true;
  mesh.receiveShadow = true;
  // scene.add( mesh );
  submesh = mesh;
  
  var origin = new THREE.Vector3( 0, 0, 0 );
  var length = 2;
  var hex = 0xad42f4;
} );

function addShadowedLight( x, y, z, color, intensity ) {

	var directionalLight = new THREE.DirectionalLight( color, intensity );
	directionalLight.position.set( x, y, z );
	scene.add( directionalLight );

	directionalLight.castShadow = true;

	var d = 1;
	directionalLight.shadow.camera.left = -d;
	directionalLight.shadow.camera.right = d;
	directionalLight.shadow.camera.top = d;
	directionalLight.shadow.camera.bottom = -d;

	directionalLight.shadow.camera.near = 1;
	directionalLight.shadow.camera.far = 4;

	directionalLight.shadow.mapSize.width = 1024;
	directionalLight.shadow.mapSize.height = 1024;

	directionalLight.shadow.bias = -0.002;

}

/* global THREE */
scene.add( new THREE.HemisphereLight( 0x443333, 0x111122 ) );

addShadowedLight( 10, 10, 10, 0xffffff, 1.35 );
addShadowedLight( -10, -10, 10, 0xffffff, 0.5 );

let targetQuat = false;

/* global Quaternian, Vector3 */
let setSubmarineRotation = (yaw, pitch, roll)=>{
  var a = new THREE.Euler( roll, pitch, yaw, 'ZYX' )
  targetQuat = (new THREE.Quaternion()).setFromEuler(a)
  // console.log(yaw, pitch, roll)
};


// ---------------------------------------
// Magnetic Field Vector
// ---------------------------------------
//     ......
//     ;;;:;
//     ;::;;;.
//     ' ':;;;;.
//         ':;;;;
//           ':;
// 
// ---------------------------------------

let lastArrow = null
let setMagnetFieldDirection = (x, y, z)=>{
  var dir = new THREE.Vector3( x, y, z );

  // screen.log(x, y, z)

  //normalize the direction vector (convert to vector of length 1)
  
  var origin = new THREE.Vector3( 0, 0, 0 );
  var length = 2//dir.length();
  var hex = 0x7923db;
  
  
  dir.normalize();
  
  if(lastArrow != null){
    scene.remove(lastArrow)
  }
  
  var arrowHelper = new THREE.ArrowHelper( dir, origin, length, hex );
  scene.add( arrowHelper );
  // console.log('after', scene)
  lastArrow = arrowHelper
};


// ---------------------------------------
// Down Vector
// ---------------------------------------
//           . ;.
//             .;
//             ;;.
//           ;.;;
//           ;;;;.
//           ;;;;;
//           ;;;;;
//           ;;;;;
//         ..;;;;;..
//           ':::::'
//             ':`

// ---------------------------------------
let lastDownArrow = null
let setDownDirection = (x, y, z)=>{
  var dir = new THREE.Vector3( -x, -y, -z );

  // console.log(x, y, z)

  //normalize the direction vector (convert to vector of length 1)
  
  
  var origin = new THREE.Vector3( 0, 0, 0 );
  var length = 2 //dir.length();
  var hex = 0xe24a09;
  
  dir.normalize();
  
  if(lastDownArrow != null){
    scene.remove(lastDownArrow)
  }
  
  var arrowHelper = new THREE.ArrowHelper( dir, origin, length, hex );
  scene.add( arrowHelper );
  // console.log('after', scene)
  lastDownArrow = arrowHelper
};
// ---------------------------------------
// Yaw Vector
// ---------------------------------------
//     ......
//     ;;;:;
//     ;::;;;.
//     ' ':;;;;.
//         ':;;;;
//           ':;
//
// 
// ---------------------------------------
let lastYawArrow = null
let setYawDirection = (x, y, z)=>{
  var dir = new THREE.Vector3( x, y, z );

  // console.log(x, y, z)

  //normalize the direction vector (convert to vector of length 1)
  
  var origin = new THREE.Vector3( 0, 0, 0 );
  var length = 2//dir.length();
  var hex = 0x0887e2;
  
  dir.normalize();
  
  if(lastYawArrow != null){
    scene.remove(lastYawArrow)
  }
  
  var arrowHelper = new THREE.ArrowHelper( dir, origin, length, hex );
  scene.add( arrowHelper );
  // console.log('after', scene)
  lastYawArrow = arrowHelper
};

// ---------------------------------------
// Forward Vector
// ---------------------------------------
//                  .
//   .. ............;;.
//     ..::::::::::::;;;;.
//  . . ::::::::::::;;:'
//                  :'
// 
// ---------------------------------------


let lastForwardArrow = null
let setForwardDirection = (x, y, z)=>{
  var dir = new THREE.Vector3( x, y, z );

  // console.log(x, y, z)

  //normalize the direction vector (convert to vector of length 1)
  
  var origin = new THREE.Vector3( 0, 0, 0 );
  var length = dir.length();
  var hex = 0xf4b642;
  
  dir.normalize();
  
  if(lastForwardArrow != null){
    scene.remove(lastForwardArrow)
  }
  
  var arrowHelper = new THREE.ArrowHelper( dir, origin, length, hex );
  scene.add( arrowHelper );
  // console.log('after', scene)
  lastForwardArrow = arrowHelper
};


// ------------------------------------------------------------------------------
// Down Plane
// ------------------------------------------------------------------------------
//                              __,:,__
//                           ,ad88P`Y88ba,
//                         ad88888' `88888ba
//                       ,d88888P'   `Y88888b,
//                      ,d888P"'       `"Y888b,
//                   __,:(["               "]):,__
//                ,ad88P`Y88ba,         ,ad88P`Y88ba,
//              ad88888' `88888ba     ad88888' `88888ba
//            ,d88888P'   `Y88888b, ,d88888P'   `Y88888b,
//           ,d888P"'       `"Y888b,d888P"'       `"Y888b,
//        __,:(["               "]):(["               "]):,__
//     ,ad88P`Y88ba,         ,ad88P`Y88ba,         ,ad88P`Y88ba,
//   ad88888' `88888ba     ad88888' `88888ba     ad88888' `88888ba
// ,d88888P'   `Y88888b, ,d88888P'   `Y88888b, ,d88888P'   `Y88888b,
// ,d888P"'       `"Y888b,d888P"'       `"Y888b,d888P"'       `"Y888b,
// :(["               "]):(["               "]):(["               "]):
// `Y88ba,         ,ad88P`Y88ba,         ,ad88P`Y88ba,         ,ad88P'
// `88888ba     ad88888' `88888ba     ad88888' `88888ba     ad88888'
//   `Y88888b, ,d88888P'   `Y88888b, ,d88888P'   `Y88888b, ,d88888P'
//     `"Y888b,d888P"'       `"Y888b,d888P"'       `"Y888b,d888P"'
//         ``":(["               "]):(["               "]):"''
//           `Y88ba,         ,ad88P'Y88ba,         ,ad88P'
//             `88888ba     ad88888' `88888ba     ad88888'
//             `Y88888b, ,d88888P'   `Y88888b, ,d88888P'
//               `"Y888b,d888P"'       `"Y888b,d888P"'
//                   ``":(["               "]):"''
//                       `Y88ba,         ,ad88P'   
//                       `88888ba     ad88888'  
//                         `Y88888b, ,d88888P'
//                           `"Y888b,d888P"'
//                               ``":"''
// ------------------------------------------------------------------------------


let lastDownPlane = null
let setDownPlane = (x, y, z)=>{
  if(lastDownPlane != null){
    scene.remove(lastDownPlane)
  }
  
  var plane = new THREE.Plane( new THREE.Vector3( -x, -y, -z ), 0 );
  var helper = new THREE.PlaneHelper( plane, 3, 0x30a858 );
  scene.add( helper );
  // console.log('after', scene)
  
  lastDownPlane = helper
};



// Render Loop
var render = function () {
  requestAnimationFrame( render );
  //
  if(targetQuat && submesh){
    //console.log('lerp', targetQuat, submesh.quaternion)
    const newQuat = submesh.quaternion.slerp(targetQuat, 0.1)
    submesh.setRotationFromQuaternion(newQuat);
  }
  
  // Render the scene
  renderer.render(scene, camera);
};

render();

