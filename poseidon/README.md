In order to emulate the poseidon submarine run the following commands

```
cd arduino/; bash emulate.sh; cd..;
cd system/; bash run.sh; cd..;
cd agent/; bash run.sh; cd..;
```