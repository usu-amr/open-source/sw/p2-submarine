module.exports = {
  dive: require('./dive'),
  tank: require('./tank'),
  imu: require('./imu'),
  pose: require('./pose'),
  killswitch: require('./killswitch'),
  power: require('./power')
}
